import java.util.Scanner;
/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/5/13
 * Time: 9:25 PM
 * P3.16 Write a Program that reads in three strings and sorts them lexicographically.
 */
public class P3_16 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter three strings: ");
        String strWord1 = in.next();
        String strWord2 = in.next();
        String strWord3 = in.next();
        String strFirst,strSecond, strThird;
        if (strWord1.compareTo(strWord2) < 0) {
            strFirst = strWord1;
            strSecond = strWord2;
        } else {
            strFirst = strWord2;
            strSecond = strWord1;
        }
        if (strWord3.compareTo(strSecond)>0) {
           strThird = strWord3;
        }
        else if (strWord3.compareTo(strFirst) < 0) {
            strThird = strSecond;
            strSecond = strFirst;
            strFirst = strWord3;
        } else {
            strThird = strSecond;
            strSecond = strWord3;
        }

        System.out.println(strFirst +"   " + strSecond + "   " + strThird);

    }

}
