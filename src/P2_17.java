
/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/5/13
 * Time: 2:08 PM
 * To change this template use File | Settings | File Templates.
 * P2.17 Write a program that reads two times in military format(0900,1730) and prints the numbers of hours and minutes
 * between the two times.
 */
import java.util.Scanner;


public class P2_17 {
    public static void main(String[] args) {
        System.out.print("Please enter the first military time:");
        Scanner scMil1 = new Scanner(System.in);
        int iMilitary1 = scMil1.nextInt();
        System.out.print("Please enter the second military time:");
        Scanner scMil2 = new Scanner(System.in);
        int iMilitary2 = scMil2.nextInt();
        int[] results;
        if (iMilitary2 >= iMilitary1) {
            results = difference(iMilitary1, iMilitary2);
        }
        else  {
            int iChanged2 = iMilitary2 + 2400;
            results = difference(iMilitary1, iChanged2);
        }
        System.out.printf("%d hours, %d minutes", results[0], results[1]);
        System.out.println();
    }

    public static int[] difference(int _iMilitary1, int _iMilitary2) {
        int iHourF = (_iMilitary2 - _iMilitary1) / 100;
        int iMinuteF = (_iMilitary2 - _iMilitary1) % 100;
        int iMinuteRecF = (_iMilitary2%100 - _iMilitary1%100) >=0 ? iMinuteF :(iMinuteF - 40) ;
        return new int[]{iHourF, iMinuteRecF};

    }


}
