/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/5/13
 * Time: 12:53 PM
 * To change this template use File | Settings | File Templates.
 * P1.10 Write a Program that print an animal speaking a greeting
 */

public class P1_10 {

    public static void main(String[] args) {


        System.out.println(" /\\---/\\    -----------");
        System.out.println("( -  -  )  /  Welcome  \\");
        System.out.println(" (  -  )  < To the Java >");
        System.out.println("(       )  \\  World!!!  /");
        System.out.println(" (--|--)    -----------");

    }
}
