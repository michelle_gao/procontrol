/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/6/13
 * Time: 3:41 PM
 * P4.18 Prime numbers. Write a program that prompts the user for an integer and then prints out all prime numbers up
 * to that integer.
 */
import java.util.Scanner;
public class P4_18 {
    public static void main(String[] args) {
        System.out.print("Please enter a Integer Number: ");
        Scanner sc = new Scanner(System.in);
        int nNumber = sc.nextInt();
        for (int i = 2; i < nNumber; i++) {
            if(nNumber%i==0) System.out.println(i);
        }
    }
}
