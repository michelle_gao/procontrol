/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/6/13
 * Time: 12:21 PM
 * P3.21 Write a program that computes the income tax according to the schedule.
 */
import java.util.Scanner;
public class P3_21 {
    public static void main(String[] args) {
        System.out.print("Please enter your year income: ");
        Scanner sc = new Scanner(System.in);
        double dIncome = sc.nextDouble();
        double dTax=0.00;
        if (dIncome <=50000) dTax = dIncome * 0.01;
        else if ((dIncome > 50000) && (dIncome <=75000))  dTax =500 + (dIncome -50000)*0.02;
        else if ((dIncome > 75000) && (dIncome <=100000)) dTax = 1000 + (dIncome - 75000)*0.03;
        else if ((dIncome >100000) && (dIncome <=250000)) dTax = 1750 + (dIncome -100000)*0.04;
        else if ((dIncome >250000) && (dIncome <=500000)) dTax = 7750 + (dIncome -250000)*0.05;
        else if (dIncome >500000) dTax = 20250 + (dIncome -100000)*0.06;
        System.out.printf("The Income Tax is: %f",dTax);

    }
}