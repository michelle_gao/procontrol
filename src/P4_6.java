/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/6/13
 * Time: 2:09 PM
 * P4.6 Translate the following pseudocode for finding the minimum value from a set of inputs into a Java program.
 */
import java.util.Scanner;
public class P4_6 {
    public static void main(String[] args) {
        System.out.print("Please enter value or \"Q\" to quit:");
        Scanner sc = new Scanner(System.in);
        double dMinimum, dValue;
        dMinimum = sc.nextDouble();
        while(sc.hasNextDouble())
        {
            dValue = sc.nextDouble();
            if(dValue < dMinimum) dMinimum = dValue;
        }
        System.out.println("The Minimum value is " + dMinimum);
    }
}
