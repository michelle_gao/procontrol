/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/5/13
 * Time: 4:03 PM
 * To change this template use File | Settings | File Templates.
 * P3.14 Write a program that takes user input describing a playing card , then print the value and shape of the card.
 */
import java.util.Scanner;

public class P3_14 {
    public static void main(String[] args) {
        System.out.print("Please enter the Card Notation:");
        Scanner sc = new Scanner(System.in);
        String strInput = sc.next();
        String _cardValue, _CardShape;
        char cCardValue,cCardShape;
        char sInput1 = (char)strInput.charAt(0);
        char sInput2 = (char)strInput.charAt(1);
        if ((sInput1 == ('1') && (sInput2 == '0'))) {
            cCardShape = (char) strInput.charAt(2);
            _cardValue = "10";
        }
        else {
            cCardValue = (char) strInput.charAt(0);
            cCardShape = (char) strInput.charAt(1);
            switch (cCardValue) {
                case 'A':
                case 'a':
                    _cardValue = "A";
                    break;
                case '2':
                    _cardValue = "2";
                    break;
                case '3':
                    _cardValue = "3";
                    break;
                case '5':
                    _cardValue = "5";
                    break;
                case '6':
                    _cardValue = "6";
                    break;
                case '7':
                    _cardValue = "7";
                    break;
                case '8':
                    _cardValue = "8";
                    break;
                case '9':
                    _cardValue = "9";
                    break;
                case 'J':
                case 'j':
                    _cardValue = "Jack";
                    break;
                case 'Q':
                case 'q':
                    _cardValue = "Queen";
                    break;
                case 'K':
                case 'k':
                    _cardValue = "King";
                    break;
                default:
                    _cardValue = "InValid Value";
                    break;
            }
        }
        switch (cCardShape) {
            case 'D':
            case 'd':
                _CardShape = "Diamonds";
                break;
            case 'H':
            case 'h':
                _CardShape = "Hearts";
                break;
            case 'S':
            case 's':
                _CardShape = "Spades";
                break;
            case 'C':
            case 'c':
                _CardShape = "Clubs";
                break;
            default:
                _CardShape = "Invalid Shape";
                break;

        }
        System.out.println(_cardValue + " of " + _CardShape);
    }
}
