/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/6/13
 * Time: 3:13 PM
 * P4.11 Write a program that reads a word and prints the number of syllables in the word. For this exercise, assume
 * that syllables are determined as follows:EAch sequence of adjacent vowels a e i o u y, except for the last e in a
 * word, is a syllable. However, if that algorithm yields a count of 0, change it  to 1.
 */
import java.util.ArrayList;
import java.util.Scanner;
public class P4_11 {
    public static void main(String[] args) {
        System.out.print("Please enter a word:");
        Scanner sc = new Scanner(System.in);
        String strWord = sc.next();
        int nSyllables=0;
        ArrayList<Character> vowChars = new ArrayList<Character>();
        vowChars.add('a');
        vowChars.add('A');
        vowChars.add('i');
        vowChars.add('I');
        vowChars.add('o');
        vowChars.add('O');
        vowChars.add('E');
        vowChars.add('e');
        vowChars.add('Y');
        vowChars.add('y');
        vowChars.add('U');
        vowChars.add('u');
        char cVow;
        for (int i = 0; i < strWord.length(); i++)  {
            cVow = strWord.charAt(i);
            if (vowChars.contains(cVow)){
                nSyllables++;
            }
        }
        char cLastLetter = strWord.charAt(strWord.length()-1);
        if(nSyllables>1) {
            if((cLastLetter=='e') || (cLastLetter == 'E') ) nSyllables--;
        }
        System.out.println("the Word " + strWord + " has " + nSyllables + " syllables.");
    }
}
