/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/6/13
 * Time: 3:48 PM
 * P3.26 Write a program that converts a positive integer into the Roman number system.
 */
import java.util.Scanner;
public class P3_26 {
    public static void main(String[] args) {
        System.out.print("Please enter a number smaller than 3999: ");
        Scanner sc = new Scanner(System.in);
        int iNumber = sc.nextInt();
        if(iNumber>3999) {
             System.out.println(" The number should not be bigger than 3999");
             return;
        }
        int nOnes=iNumber%10;
        String strRomanOnes = convertRoman(nOnes,new char[]{'I','V','X'});
        int nTens =(iNumber%100 -nOnes)/10;
        String strRomanTens = convertRoman(nTens,new char[]{'X','L','C'});
        int nHundreds = (iNumber%1000 - nTens - nOnes)/100;
        String strRomanHundreds = convertRoman(nHundreds,new char[]{'C','D','M'});
        int nThousands = iNumber/1000;
        String strRomanThousands = convertRoman(nThousands, new char[]{'M',' ',' '});
        System.out.println("The Roman numerals are: " + strRomanThousands + strRomanHundreds +strRomanTens +strRomanOnes);
    }
    public static String convertRoman(int nNUmber, char[] cRomanLetter) {
        char cRomanLetter1 = cRomanLetter[0];
        char cRomanLetter5 = cRomanLetter[1];
        char cRomanLetter10 = cRomanLetter[2];
        String strRoman="";
        switch(nNUmber) {
            case 1:
                strRoman = "" + cRomanLetter1;break;
            case 2:
                strRoman = "" + cRomanLetter1 + cRomanLetter1;break;
            case 3:
                strRoman = "" + cRomanLetter1 + cRomanLetter1 + cRomanLetter1;break;
            case 4:
                strRoman = "" + cRomanLetter1 + cRomanLetter5; break;
            case 5:
                strRoman = "" + cRomanLetter5; break;
            case 6:
                strRoman = "" + cRomanLetter5 + cRomanLetter1; break;
            case 7:
                strRoman = "" + cRomanLetter5 + cRomanLetter1 + cRomanLetter1;break;
            case 8:
                strRoman = "" + cRomanLetter5 + cRomanLetter1 + cRomanLetter1 + cRomanLetter1; break;
            case 9:
                strRoman = "" + cRomanLetter1 + cRomanLetter10;break;
            default: break;
        }
        return strRoman;
    }
}
