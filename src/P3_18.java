/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/6/13
 * Time: 11:39 AM
 * P3.18 Write a program that prompts the user for a month and day and the prints the season.
 */
import java.util.Scanner;
public class P3_18 {
    public static void main(String[] args) {
        System.out.print("Please enter a month and a day: ");
        Scanner sc = new Scanner(System.in);
        int iMonth = sc.nextInt();
        int iDay = sc.nextInt();
        String strSeason = "An inValid Season";
        if (iMonth == 1 || iMonth ==2 | iMonth ==3 ) strSeason = "Winter";
        else if (iMonth == 5 || iMonth ==6 || iMonth == 7) strSeason = "Spring";
        else if (iMonth == 7 || iMonth ==8 || iMonth == 9 ) strSeason = "Summer";
        else if (iMonth == 10 || iMonth ==11 || iMonth ==12 ) strSeason = "Fall";
        if ((iMonth % 3) == 0 && (iDay >= 21)) {
            if (strSeason.equals("Winter")) strSeason = "Spring";
            else if (strSeason.equals("Spring")) strSeason = "Summer";
            else if (strSeason.equals("Summer")) strSeason = "Fall";
            else if (strSeason.equals("Fall")) strSeason = "Winter";
        }
        System.out.println("The season is: " + strSeason);
    }
}
